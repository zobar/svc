package svc

import "fmt"

type CastErr struct {
	Iface string
	Obj   Obj
}

type DriverSetErr struct{}
type NoDriverErr struct{}

func (err CastErr) Error() string {
	return fmt.Sprintf("%q is not a %q", err.Obj.Path(), err.Iface)
}

func (err DriverSetErr) Error() string {
	return fmt.Sprintf("D-Bus driver already set")
}

func (err NoDriverErr) Error() string {
	return fmt.Sprintf("No D-Bus driver set")
}
