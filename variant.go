package svc

type Variant interface{}

func NewVariant(val interface{}) Variant {
	return GetDriver().NewVariant(val)
}
