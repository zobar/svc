package guelfey

import (
	"github.com/guelfey/go.dbus"
	"github.com/zobar/svc"
)

type converter struct {
	objs func([]string) []svc.Obj
}

func (converter *converter) convert(val interface{}) (out interface{}) {
	switch in := val.(type) {
	case dbus.ObjectPath:
		out = converter.objs([]string{(string)(in)})[0]
	case dbus.Variant:
		out = converter.convert(in.Value())
	case []dbus.ObjectPath:
		out = converter.convertPaths(in)
	case map[string]dbus.Variant:
		out = converter.convertMap(in)
	default:
		out = in
	}
	return
}

func (converter *converter) convertMap(vals map[string]dbus.Variant) (outs map[string]interface{}) {
	outs = make(map[string]interface{}, len(vals))
	for key, val := range vals {
		outs[key] = converter.convert(val)
	}
	return
}

func (converter *converter) convertPaths(vals []dbus.ObjectPath) []svc.Obj {
	strings := make([]string, len(vals))
	for i, in := range vals {
		strings[i] = string(in)
	}
	return converter.objs(strings)
}

func (converter *converter) convertSlice(vals []interface{}) (outs []interface{}) {
	outs = make([]interface{}, len(vals))
	for i, in := range vals {
		outs[i] = converter.convert(in)
	}
	return
}

func (conn *conn) converter(dest string) *converter {
	return &converter{conn.bus.Svc(dest).Objs}
}
