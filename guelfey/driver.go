package guelfey

import (
	"github.com/guelfey/go.dbus"
	"github.com/zobar/svc"
)

type driver struct {}

func Use() {
	svc.SetDriver(&driver{})
}

func (driver *driver) Sys(bus svc.Bus) svc.Conn {
	sys := &conn{
		bus:      bus,
		connChan: make(chan connCall, 1),
	}
	go sys.init()
	return sys
}

func (driver *driver) NewVariant(val interface{}) svc.Variant {
	return dbus.MakeVariant(val)
}
