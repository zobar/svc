package guelfey

type result struct {
	conn *conn
	dest string
	done chan bool
	err  error
	val  []interface{}
}

func (result *result) Err() error {
	result.wait()
	return result.err
}

func (result *result) Val(i int) interface{} {
	result.wait()
	return result.conn.converter(result.dest).convert(result.val[i])
}

func (result *result) wait() {
	<-result.done
}
