package guelfey

import (
	"github.com/guelfey/go.dbus"
	"github.com/guelfey/go.dbus/introspect"
	"github.com/zobar/svc"
	"strings"
)

type (
	connCall struct {
		conn *dbus.Conn
		err  error
	}

	conn struct {
		bus      svc.Bus
		connChan chan connCall
		connCall connCall
	}
)

var sys *conn

func (conn *conn) Call(dest, path, iface, method string, args []interface{}) svc.Result {
	result := &result{dest: dest, done: make(chan bool), conn: conn}
	if dbusConn, err := conn.conn(); err != nil {
		result.err = err
		close(result.done)
	} else {
		go func() {
			obj := dbusConn.Object(dest, (dbus.ObjectPath)(path))
			call := obj.Call(iface+"."+method, 0, args...)
			result.err = call.Err
			result.val = call.Body
			close(result.done)
		}()
	}
	return result
}

func (conn *conn) conn() (*dbus.Conn, error) {
	for conn.connCall = range conn.connChan {
	}
	return conn.connCall.conn, conn.connCall.err
}

func (conn *conn) Ifaces(dest, path string) (ifaces []string, err error) {
	obj, err := conn.obj(dest, path)
	if err == nil {
		var node *introspect.Node
		if node, err = introspect.Call(obj); err == nil {
			ifaces = make([]string, len(node.Interfaces))
			for i, iface := range node.Interfaces {
				ifaces[i] = iface.Name
			}
		}
	}
	return
}

func (conn *conn) init() {
	dbusConn, err := dbus.SystemBus()
	if err == nil {
		if err = dbusConn.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, "type='signal'").Err; err == nil {
			signalChan := make(chan *dbus.Signal, 8)
			go conn.recv(signalChan)
			dbusConn.Signal(signalChan)
		}
	}
	conn.connChan <- connCall{conn: dbusConn, err: err}
	close(conn.connChan)
}

func (conn *conn) obj(dest, path string) (obj *dbus.Object, err error) {
	dbusConn, err := conn.conn()
	if err == nil {
		obj = dbusConn.Object(dest, (dbus.ObjectPath)(path))
	}
	return
}

func (conn *conn) recv(signalChan <-chan *dbus.Signal) {
	for signal := range signalChan {
		name := signal.Name
		path := string(signal.Path)
		dot := strings.LastIndex(name, ".")
		ifaceName, method := name[:dot], name[dot+1:]
		for _, svc := range conn.bus.Svcs() {
			if iface := svc.Find(path, ifaceName); iface != nil {
				args := conn.converter(svc.Name()).convertSlice(signal.Body)
				iface.Signal(method, args)
			}
		}
	}
}
