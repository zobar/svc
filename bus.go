package svc

import "sync"

type Bus interface {
	Conn() Conn
	Svc(name string) Svc
	Svcs() []Svc
}

type bus struct {
	conn     Conn
	svcs     map[string]Svc
	svcsLock sync.Mutex
}

var (
	sys     *bus
	sysLock = sync.Mutex{}
)

func Sys() Bus {
	sysLock.Lock()
	defer sysLock.Unlock()
	if sys == nil {
		sys = &bus{
			svcs:     map[string]Svc{},
			svcsLock: sync.Mutex{},
		}
		sys.conn = GetDriver().Sys(sys)
	}
	return sys
}

func (bus *bus) Conn() Conn {
	return bus.conn
}

func (bus *bus) Svc(name string) (svc Svc) {
	bus.svcsLock.Lock()
	defer bus.svcsLock.Unlock()
	svc, ok := bus.svcs[name]
	if !ok {
		svc = newSvc(bus, name)
		bus.svcs[name] = svc
	}
	return
}

func (bus *bus) Svcs() (svcs []Svc) {
	bus.svcsLock.Lock()
	defer bus.svcsLock.Unlock()
	svcs = make([]Svc, 0, len(bus.svcs))
	for _, svc := range bus.svcs {
		svcs = append(svcs, svc)
	}
	return
}
