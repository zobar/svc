package svc

import "sync"

type Conn interface {
	Call(dest, path, iface, method string, args []interface{}) Result
	Ifaces(dest, path string) ([]string, error)
}

type Driver interface {
	NewVariant(val interface{}) Variant
	Sys(bus Bus) Conn
}

var (
	driver     Driver
	driverLock = sync.Mutex{}
)

func GetDriver() Driver {
	driverLock.Lock()
	defer driverLock.Unlock()
	if driver == nil {
		panic(NoDriverErr{})
	}
	return driver
}

func SetDriver(d Driver) {
	driverLock.Lock()
	defer driverLock.Unlock()
	if driver == nil {
		driver = d
	} else if driver != d {
		panic(DriverSetErr{})
	}
}
