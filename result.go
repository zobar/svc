package svc

type (
	Result interface {
		Err() error
		Val(i int) interface{}
	}
)
