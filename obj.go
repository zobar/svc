package svc

type ifacesCall struct {
	ifaces map[string]Iface
	err    error
}

type Obj interface {
	As(name string) (Iface, error)
	Path() string
	Svc() Svc
}

type obj struct {
	ifacesChan chan bool
	ifacesCall ifacesCall
	path       string
	svc        Svc
}

func newObj(svc Svc, path string) Obj {
	obj := &obj{
		ifacesChan: make(chan bool),
		path:       path,
		svc:        svc,
	}
	go obj.init()
	return obj
}

func (obj *obj) As(name string) (iface Iface, err error) {
	ifaces, err := obj.ifaces()
	if err == nil {
		var ok bool
		if iface, ok = ifaces[name]; !ok {
			err = CastErr{name, obj}
		}
	}
	return
}

func (obj *obj) ifaces() (ifaces map[string]Iface, err error) {
	<-obj.ifacesChan
	return obj.ifacesCall.ifaces, obj.ifacesCall.err
}

func (obj *obj) init() {
	var (
		ifaces          map[string]Iface
		svc             = obj.svc
		ifaceNames, err = svc.Bus().Conn().Ifaces(svc.Name(), obj.path)
	)
	if err == nil {
		ifaces = make(map[string]Iface, len(ifaceNames))
		for _, iface := range ifaceNames {
			ifaces[iface] = newIface(obj, iface)
		}
	}
	obj.ifacesCall = ifacesCall{ifaces, err}
	close(obj.ifacesChan)
}

func (obj *obj) Path() string {
	return obj.path
}

func (obj *obj) Svc() Svc {
	return obj.svc
}
