package svc

import "sync"

type (
	Svc interface {
		Bus() Bus
		Find(path, ifaceName string) Iface
		Name() string
		Obj(path string) Obj
		Objs(paths []string) []Obj
	}

	svc struct {
		bus      Bus
		name     string
		objs     map[string]Obj
		objsLock sync.Mutex
	}
)

func newSvc(bus Bus, name string) Svc {
	return &svc{
		bus:      bus,
		name:     name,
		objs:     map[string]Obj{},
		objsLock: sync.Mutex{},
	}
}

func (svc *svc) Bus() Bus {
	return svc.bus
}

func (svc *svc) Find(path, ifaceName string) (iface Iface) {
	svc.objsLock.Lock()
	defer svc.objsLock.Unlock()
	if obj, ok := svc.objs[path]; ok {
		iface, _ = obj.As(ifaceName)
	}
	return
}

func (svc *svc) Name() string {
	return svc.name
}

func (svc *svc) Obj(path string) Obj {
	return svc.Objs([]string{path})[0]
}

func (svc *svc) Objs(paths []string) (objs []Obj) {
	objs = make([]Obj, len(paths))
	svc.objsLock.Lock()
	defer svc.objsLock.Unlock()
	for i, path := range paths {
		obj, ok := svc.objs[path]
		if !ok {
			obj = newObj(svc, path)
			svc.objs[path] = obj
		}
		objs[i] = obj
	}
	return
}
