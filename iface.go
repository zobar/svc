package svc

import "sync"

type (
	Iface interface {
		Call(method string, args ...interface{}) Result
		Obj() Obj
		On(name string, handler func(args []interface{}))
		Signal(name string, args []interface{})
	}

	iface struct {
		handlerLock sync.Mutex
		handlers    map[string][]func([]interface{})
		name        string
		obj         Obj
	}
)

func newIface(obj Obj, name string) Iface {
	return &iface{
		handlerLock: sync.Mutex{},
		handlers:    map[string][]func([]interface{}){},
		name:        name,
		obj:         obj,
	}
}

func (iface *iface) Call(method string, args ...interface{}) Result {
	obj := iface.obj
	svc := obj.Svc()
	conn := svc.Bus().Conn()
	return conn.Call(svc.Name(), obj.Path(), iface.name, method, args)
}

func (iface *iface) Obj() Obj {
	return iface.obj
}

func (iface *iface) On(name string, handler func([]interface{})) {
	iface.handlerLock.Lock()
	defer iface.handlerLock.Unlock()
	iface.handlers[name] = append(iface.handlers[name], handler)
}

func (iface *iface) Signal(name string, args []interface{}) {
	iface.handlerLock.Lock()
	defer iface.handlerLock.Unlock()
	for _, handler := range iface.handlers[name] {
		go handler(args)
	}
}
